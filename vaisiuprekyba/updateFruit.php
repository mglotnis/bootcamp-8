<?php
	
	require('db/connection.php');

	$id = $_GET['num'];

	$stmt = $conn->prepare("SELECT * FROM prekes WHERE id=$id");
	$stmt->execute();

	$result = $stmt->fetch();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Atnaujinkite nauja vaisiu</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

	<div class="container">
		<h1>Atnaujinkite vaisiu</h1>

		<form action="db/update.php" method="GET">
			<input type="hidden" name="num" value="<?php echo $id; ?>">
			<div class="form-group">
				<label>Pavadinimas</label>
				<input type="text" name="vaisius" class="form-control"
				value="<?php echo $result['vaisius']; ?>">
			</div>
			<div class="form-group">
				<label>Kiekis</label>
				<input type="text" name="kiekis" class="form-control" value="<?php echo $result['kiekis']; ?>">
			</div>
			<div class="form-group">
				<label>Kaina</label>
				<input type="text" name="kaina" class="form-control" value="<?php echo $result['kaina']; ?>">
			</div>
			<div class="form-group">
				<button class="btn btn-info">Iterpti</button>
			</div>
		</form>
	</div>

</body>
</html>