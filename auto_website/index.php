<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Naujas projektas</title>
	
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="dist/css/main.min.css">
</head>
<body>
	
	<div class="container-fluid page">
		<div class="navigation">
			<a href="#" class="btn btn-default">Pradžia</a>
			<a href="auth/loginView.php" class="btn btn-info">Login</a>
			<a href="auth/registerView.php" class="btn btn-warning">Register</a>
		</div>

		<div class="container">
			<h1>TODO:</h1>
			<ol>
				<li>Susikurti duomenų bazę: auto_db</li>
				<li>Susikurti lentelę: users ir cars</li>
				<li>PHP kodas prisijungimui jau padarytas</li>

				<li>Sutvarkyti registracijos kodą</li>
				<ul>
					<li>Padaryti bent dviejų tipų vartotojus: user ir admin</li>
					<li>Užkoduoti slaptažodžius su bcrypt</li>
				</ul>
			
				<li>Sutvarkyti prisijungimo kodą</li>
				<ul>
					<li>Atkoduoti slaptažodį ir palyginti su esančiu DB</li>
					<li>Pagal vartotojo tipą nukreipti vartotoją į jo vietą</li>
				</ul>

				<li>Neleisti neprisijungusiam vartotojui patekti į admin/user papkes.</li>
				<li>Neleisti prisijungusiam user patekti į admin</li>

			</ol>
		</div>
	
	</div>

	
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	<script src="dist/js/app.min.js"></script>
</body>
</html>