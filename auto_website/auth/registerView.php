<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registracijos forma</title>
	
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="../dist/css/main.min.css">
</head>
<body>
	
	<div class="container-fluid page">
		<div class="navigation">
			<a href="../index.php" class="btn btn-default">Pradžia</a>
			<a href="loginView.php" class="btn btn-info">Login</a>
			<a href="#" class="btn btn-warning">Register</a>
		</div>

		<div class="container">
			<h1>Registracijos forma</h1>
			<form>
				<div class="form-group">
					<label>Vardas, pavarde</label>
					<input type="text" name="email" class="form-control">
				</div>
				<div class="form-group">
					<label>El. pastas</label>
					<input type="text" name="email" class="form-control">
				</div>
				<div class="form-group">
					<label>Slaptazodis</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group">
					<button class="btn btn-warning">
						Prisijungti
					</button>
				</div>
			</form>
			
		</div>
	</div>

	

	
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	<script src="dist/js/app.min.js"></script>
</body>
</html>