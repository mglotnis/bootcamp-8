// select canvas tag
var canvas = document.querySelector('canvas');
// set context so we could draw shapes on canvas
var c = canvas.getContext('2d');

// Set canvas height and width to window size
var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;


// Generate random number between given min and max values
function random(min, max) {
	return num = Math.floor(Math.random() * (max - min + 1)) + 1;
}

// Our constructor
// We pass position in x and y coordinates
// Velocity - for moving, color and size
function Ball(x, y, velX, velY, color, size) {
	this.x = x;
	this.y = y;
	this.velX = velX;
	this.velY = velY;
	this.color = color;
	this.size = size;
}

// function to draw our ball
// beginPath - begin drawing shape
// shape color with fillStyle
// Draw our ball at x and y coordinates,
// ball radius - size
// start and end deggrees, 0 
// and fill 
Ball.prototype.draw = function() {
  // c.clearRect(0, 0, width, height);
	c.beginPath();
	c.fillStyle = this.color;
	c.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
	c.fill();
};	


// // create our ball and test it
// var firstBall = new Ball(100, 100, 50, 50, 'orange', 50 );
// firstBall.draw();


// Update function check canvas border and move within it
Ball.prototype.update = function() {
  if ((this.x + this.size) >= width) {
    this.velX = -(this.velX);
  }

  if ((this.x - this.size) <= 0) {
    this.velX = -(this.velX);
  }

  if ((this.y + this.size) >= height) {
    this.velY = -(this.velY);
  }

  if ((this.y - this.size) <= 0) {
    this.velY = -(this.velY);
  }

  this.x += this.velX;
  this.y += this.velY;
}


// empty balls array
var balls = [];


//loop function
function loop() {


	// add random balls to array
  while (balls.length < 25) {
    var ball = new Ball(random(0,width), random(0,height), random(-7,7), random(-7,7),
      'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) +')',
      random(10,20)
    );
    balls.push(ball);
  }

  //draw ball and update
  for (var i = 0; i < balls.length; i++) {
    balls[i].draw();
    balls[i].update();
  }


  // call loop function again
  requestAnimationFrame(loop);
}


// start it once and see te magic
loop();

